//
//  MapInteractor.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/1/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//


protocol MapInteractorOutputs: AnyObject {
    func onSuccessFetch(res: [CarListingEntity])
    func onErrorFetch(error: Error)
}

final class MapInteractor: Interactorable {

    weak var presenter: MapInteractorOutputs?

    func fetchCars() {
        CarListingApi().fetch(with: [:], onSuccess: { [weak self] res in
                 self?.presenter?.onSuccessFetch(res: res)
                 //print(res)
             }) { [weak self] error in
                 self?.presenter?.onErrorFetch(error: error)
                 //print(error)
             }
    }
}
