//
//  BottomMapView.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/5/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import UIKit


@IBDesignable
class BottomMapView: UIView {

    @IBOutlet weak var carMakeLabel: UILabel!
    @IBOutlet weak var carImage: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func fill(with data: CarListingEntity)
    {
        carMakeLabel?.text = "\(data.make ?? "") \(data.modelName ?? "")"
        carMakeLabel.setBackgroundColor(withColor: UIColor.primary())
        carImage.kf.setImage(
                   with: URL(string: data.carImageUrl ?? ""),
                   placeholder: UIImage(named: "car-placeholder"),
                   options: [
                       .scaleFactor(UIScreen.main.scale),
                       .transition(.fade(1)),
                       .cacheOriginalImage
                   ])
    }
    
    

}
