//
//  MapEntities.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/2/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation
import GoogleMaps

struct MapEntryEntity {
}

final class MapEntities {
    let entryEntity: MapEntryEntity
    var carListingEntity: [CarListingEntity] = []
    var carMarkers: [GMSMarker] = []

    class FetchApiState {
        var pageCount = 1
        var isFetching = false
    }

    var fetchApiState = FetchApiState()

    init(entryEntity: MapEntryEntity) {
        self.entryEntity = entryEntity
    }
}
