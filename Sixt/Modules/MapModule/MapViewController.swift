//
//  MapViewController.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/1/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher
import NVActivityIndicatorView



protocol MapViewInputs: AnyObject {
   func configureGMSMap()
   func reloadMapLocations(markers: [GMSMarker])
   func showProgressIndicator(visible: Bool)
   func showBottomDetailedView(with data: CarListingEntity)
}

protocol MapViewOutputs: AnyObject {
    func viewDidLoad()
    func viewWillAppear()
    func didTapListCars()
   // func onCloseButtonTapped()
}


final class MapViewController: UIViewController {
    internal var presenter: MapViewOutputs?
    internal var gmsViewDelegate: GMSViewDelegate?
    private var bottomDetailedView: BottomMapView!
    @IBOutlet weak var bottomBarView: UIView!
    @IBOutlet weak var progressIndicator: NVActivityIndicatorView!
    @IBOutlet weak var mapView: GMSMapView! {
        didSet {
            mapView.delegate = self
        }
    }
    private var bottomViewVisible = false
    
    @IBAction func didTapCarListing(_ sender: Any) {
        presenter?.didTapListCars()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progressIndicator.type = .lineScale
        progressIndicator.color = UIColor.primary()
        presenter?.viewDidLoad()
        bottomDetailedView = BottomMapView.fromNib(nibName: "BottomMapView")

            
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        presenter?.viewWillAppear()
    }
}

extension MapViewController: MapViewInputs {
    func configureGMSMap() {
        //"latitude": 48.134557,"longitude": 11.576921,
        let camera = GMSCameraPosition.camera(withLatitude: 48.13, longitude: 11.567, zoom: 12.0)
        //mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.mapView.camera = camera
        do {
                if let styleURL = Bundle.main.url(forResource: "darkGMS-style", withExtension: "json") {
                    mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                    
                } else {
                    print("Unable to find style.json")
                }
            } catch {
                print("The style definition could not be loaded: \(error)")
            }
    }
    
    
    func showBottomDetailedView(with data: CarListingEntity)
    {

        if(!bottomViewVisible)
        {
            var bottomSafeArea: CGFloat
            if #available(iOS 11.0, *) {
                bottomSafeArea = view.safeAreaInsets.bottom
            } else {
                bottomSafeArea = bottomLayoutGuide.length
            }
            let startY = self.view.frame.maxY - bottomSafeArea - 80 - self.bottomBarView.frame.height
            bottomDetailedView.frame = CGRect(x: 0, y: startY, width: self.view.frame.width, height: 80.0)
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromTop
            bottomDetailedView.layer.add(transition, forKey: nil)
            bottomDetailedView.backgroundColor = UIColor.lightGrey()
            self.view.insertSubview(bottomDetailedView, aboveSubview: self.mapView)
            bottomViewVisible = true
        }
        bottomDetailedView.fill(with: data)

        //self.view.addSubview(bottomDetailedView)
    }
    
    func showProgressIndicator(visible: Bool)
       {
        progressIndicator.alpha = visible ? 1.0 : 0
        visible ? progressIndicator.startAnimating() : progressIndicator.stopAnimating()
       }
    
    func reloadMapLocations(markers: [GMSMarker]) {
        DispatchQueue.main.async { [weak self] in
            for marker in markers
            {
                marker.map = self?.mapView
            }
        }
     }
}

extension MapViewController: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        gmsViewDelegate?.mapView(mapView: mapView, marker: marker)
        return true
    }
}

extension MapViewController: Viewable {}

