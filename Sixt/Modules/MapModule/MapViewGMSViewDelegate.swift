//
//  MapViewGMSViewDelegate.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/4/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

protocol MapViewGMSViewDelegateOutputs: AnyObject {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker)

}

final class MapViewGMSViewDelegate: GMSViewDelegate {
  private var entities: MapEntities!
    private weak var presenter: MapViewGMSViewDelegateOutputs?

    init(entities: MapEntities, presenter: MapViewGMSViewDelegateOutputs) {
        self.entities = entities
        self.presenter = presenter
    }


    func mapView(mapView: GMSMapView, marker: GMSMarker) {
        presenter?.mapView(mapView, didTap: marker)
    }

}
