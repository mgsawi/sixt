//
//  MapPresenter.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/1/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation
import GoogleMaps

typealias MapPresenterDependencies = (
    interactor: MapInteractor,
    router: MapRouterOutput
)

final class MapPresenter: Presenterable {

   // internal var entities: ListEntities
    private weak var view: MapViewInputs!
    let dependencies: MapPresenterDependencies
    internal var entities: MapEntities
    internal var previouslySelectedMarker: GMSMarker!

    init(entities: MapEntities, view: MapViewInputs,
         dependencies: MapPresenterDependencies)
    {
        self.view = view
        self.entities = entities
        self.dependencies = dependencies
    }

}

extension MapPresenter: MapViewOutputs {
    func viewDidLoad() {
        view.showProgressIndicator(visible: true)
        view.configureGMSMap()
        entities.fetchApiState.isFetching = true
        dependencies.interactor.fetchCars()
    }
    
    func didTapListCars() {
        print(entities.carListingEntity)
        self.dependencies.router.transitionCarListing(carListingEntity: entities.carListingEntity)
    }
    
    func viewWillAppear() {
        
    }
}

extension MapPresenter: MapInteractorOutputs {

    func onSuccessFetch(res: [CarListingEntity]) {
        print("Loaded Cars Successfully")
        entities.fetchApiState.isFetching = false
        entities.carListingEntity = res
        for data in res {
                 if let lat = data.latitude, let long = data.longitude {
                     let location = CLLocationCoordinate2D(latitude:             CLLocationDegrees(exactly: lat)!, longitude: CLLocationDegrees(exactly: long)!)
                                     
                        let marker = GMSMarker()
                        marker.position = location
                        marker.userData = data
                        marker.snippet = data.name!
                        marker.icon = UIImage(named: "marker-default")
                        entities.carMarkers.append(marker)                     
                 } else {
                     print("Unable to extract coordinates for the selected item")
                 }
             }
        view.reloadMapLocations(markers: entities.carMarkers)
        view.showProgressIndicator(visible: false)

    }
    
    func onErrorFetch(error: Error) {
        print("Failed to load Cars Successfully")
        entities.fetchApiState.isFetching = false
        view.showProgressIndicator(visible: false)
    }
}
    
extension MapPresenter: MapViewGMSViewDelegateOutputs {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) {

        view.showBottomDetailedView(with: marker.userData as! CarListingEntity)
        if(previouslySelectedMarker != marker)
        {
            marker.icon = UIImage(named: "marker-selected")
            if previouslySelectedMarker != nil
            {
                previouslySelectedMarker.icon = UIImage(named: "marker-default")
            }
            previouslySelectedMarker = marker
        }
        let camera = GMSCameraPosition.camera(withLatitude: marker.position.latitude, longitude: marker.position.longitude, zoom: mapView.camera.zoom < 15.0 ? 18 : mapView.camera.zoom)
        mapView.animate(to: camera)
        
    }
}
