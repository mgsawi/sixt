//
//  MapRouter.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/1/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation
import UIKit

struct MapRouterInput {

    private func view(entryEntity: MapEntryEntity) -> MapViewController {
        let view = MapViewController()
        let interactor = MapInteractor()
        let dependencies = MapPresenterDependencies(interactor: interactor, router: MapRouterOutput(view))
        let presenter = MapPresenter(entities: MapEntities(entryEntity: entryEntity), view: view, dependencies: dependencies)
        view.presenter = presenter
        view.gmsViewDelegate = MapViewGMSViewDelegate(entities: presenter.entities, presenter: presenter)

        interactor.presenter = presenter
        return view
    }

    func push(from: Viewable, entryEntity: MapEntryEntity) {
        let view = self.view(entryEntity: entryEntity)
        from.push(view, animated: true)
    }

    func present(from: Viewable, entryEntity: MapEntryEntity) {
        let mapVC = view(entryEntity: entryEntity)
        mapVC.modalPresentationStyle = .fullScreen
        from.present(mapVC, animated: true)
    }
}

final class MapRouterOutput: Routerable {

    private(set) weak var view: Viewable!

    init(_ view: Viewable) {
        self.view = view
    }
    
    func transitionCarListing(carListingEntity: [CarListingEntity]) {
        CarListingRouterInput().present(from: view, entryEntity: CarListingEntryEntity(carListingEntity: carListingEntity))
       }

}


