//
//  CarListingRouter.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/3/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation
import UIKit

struct CarListingRouterInput {

    private func view(entryEntity: CarListingEntryEntity) -> CarListingViewController {
        let view = CarListingViewController()
        let interactor = CarListingInteractor()
        let dependencies = CarListingPresenterDependencies(interactor: interactor, router: CarListingRouterOutput(view))
        let presenter = CarListingPresenter(entities: CarListingEntities(entryEntity: entryEntity), view: view, dependencies: dependencies)
        view.tableViewDataSource = CarListingViewDataSource(entities: presenter.entities, presenter: presenter)
        view.presenter = presenter
        interactor.presenter = presenter
        return view
    }

    func push(from: Viewable, entryEntity: CarListingEntryEntity) {
        let view = self.view(entryEntity: entryEntity)
        from.push(view, animated: true)
    }

    func present(from: Viewable, entryEntity: CarListingEntryEntity) {
        let mapVC = view(entryEntity: entryEntity)
        mapVC.modalPresentationStyle = .pageSheet
        from.present(mapVC, animated: true)
    }
}

final class CarListingRouterOutput: Routerable {

    private(set) weak var view: Viewable!

    init(_ view: Viewable) {
        self.view = view
    }

}


