//
//  CarListingItemCell.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/3/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import UIKit
import WebKit

class CarListingItemCell: UITableViewCell
{
    @IBOutlet weak var carMakeLabel: UILabel!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var transmissionTypeIcon: UIImageView!
    @IBOutlet weak var transmissionTypeLabel: UILabel!
    @IBOutlet weak var carLocationWebView: WKWebView!
    
}
