//
//  CarListingEntities.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/3/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

struct CarListingEntryEntity {
    let carListingEntity: [CarListingEntity]
    init(carListingEntity: [CarListingEntity]) {
        self.carListingEntity = carListingEntity
    }
}

struct CarListingEntities {
    let entryEntity: CarListingEntryEntity

    init(entryEntity: CarListingEntryEntity) {
        self.entryEntity = entryEntity
    }
}
