//
//  CarListingPresenter.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/3/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation

typealias CarListingPresenterDependencies = (
    interactor: CarListingInteractor,
    router: CarListingRouterOutput
)

final class CarListingPresenter: Presenterable {

   // internal var entities: ListEntities
    private weak var view: CarListingInputs!
    let dependencies: CarListingPresenterDependencies
    internal var entities: CarListingEntities

    init(entities: CarListingEntities, view: CarListingInputs,
         dependencies: CarListingPresenterDependencies)
    {
        self.view = view
        self.entities = entities
        self.dependencies = dependencies
    }

}

extension CarListingPresenter: CarListingOutputs {
    func onCloseButtonTapped() {
        self.dependencies.router.dismiss(animated: true)
    }
    
    func reloadTableView(tableViewDataSource: CarListingViewDataSource) {
        self.view.reloadTableView()
    }
    
    func viewDidLoad() {
      
    }
    
    func viewWillAppear() {
        
    }
    
}

extension CarListingPresenter: CarListingInteractorOutputs {

}

extension CarListingPresenter: CarListingViewDataSourceOutputs {
    func didSelect(_ carListingEntity: CarListingEntity) {
        
    }
    
 
}



