//
//  CarDetailViewController.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/3/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

protocol CarListingInputs: AnyObject {
    func reloadTableView()
}

protocol CarListingOutputs: AnyObject {
    func viewDidLoad()
    func viewWillAppear()
    func onCloseButtonTapped()
}

final class CarListingViewController: UIViewController {
    internal var presenter: CarListingOutputs?
    internal var tableViewDataSource: CarListingViewDataSource?

    @IBOutlet private weak var tableView: UITableView! {
           didSet {
               let nib = UINib(nibName: "CarListingItemCell", bundle: nil)
               tableView.register(nib, forCellReuseIdentifier: "CarListingItemCell")
               tableView.delegate = self
               tableView.dataSource = self
                tableView.emptyDataSetView { view in
                view.titleLabelString(NSAttributedString(string: "OOPS"))
                .detailLabelString(NSAttributedString(string: "Unable to connect to the server"))
                .image(UIImage(named: "server-connection"))
                    
            }
           }
       }
    @IBAction func didTapCloseButton(_ sender: Any) {
        presenter?.onCloseButtonTapped()
    }
    
    override func viewDidLoad() {
        presenter?.viewDidLoad()
    }
    
        

}

extension CarListingViewController: CarListingInputs {

    func reloadTableView() {
              DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
              }
    }
}

extension CarListingViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewDataSource?.numberOfItems ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableViewDataSource?.itemCell(tableView: tableView, indexPath: indexPath) ?? UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        tableViewDataSource?.didSelect(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120
    }
}

extension CarListingViewController: Viewable {}
