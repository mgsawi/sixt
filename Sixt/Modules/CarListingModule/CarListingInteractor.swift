//
//  CarListingInteractor.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/3/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//


protocol CarListingInteractorOutputs: AnyObject {

}

final class CarListingInteractor: Interactorable {
    weak var presenter: CarListingInteractorOutputs?
}

