//
//  CarListingTableViewDataSource.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/4/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation
import UIKit

protocol CarListingViewDataSourceOutputs: AnyObject {
    func didSelect(_ carListingEntity: CarListingEntity)
}

final class CarListingViewDataSource: TableViewItemDataSource {

    private var entities: CarListingEntities!
    private weak var presenter: CarListingViewDataSourceOutputs?

    init(entities: CarListingEntities, presenter: CarListingViewDataSourceOutputs) {
        self.entities = entities
        self.presenter = presenter
    }

    var numberOfItems: Int {
        print(entities.entryEntity.carListingEntity.count)
        return entities.entryEntity.carListingEntity.count
    }

    func itemCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let car = entities.entryEntity.carListingEntity[safe: indexPath.row] else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarListingItemCell", for: indexPath) as! CarListingItemCell
        cell.carMakeLabel?.text = "\(car.make ?? "") \(car.modelName ?? "")"
        cell.carMakeLabel.setBackgroundColor(withColor: UIColor.primary())
        cell.carImage.kf.setImage(
            with: URL(string: car.carImageUrl ?? ""),
            placeholder: UIImage(named: "car-placeholder"),
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        cell.transmissionTypeIcon.image = (car.transmission == "A") ? UIImage(named: "automatic-gear") : UIImage(named: "manual-gear")
        cell.transmissionTypeLabel.text = (car.transmission == "A") ? "Automatic" : "Manual"
        return cell
    }

    func didSelect(tableView: UITableView, indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
