//
//  UIColor+Extension.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/2/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import UIKit

extension UIColor {
    class func primary() -> UIColor {
        return UIColor(red: 255.0/255.0, green: 95.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    }
    class func darkGrey() -> UIColor {
        return UIColor(red: 25.0/255.0, green: 25.0/255.0, blue: 25.0/255.0, alpha: 1.0)
    }
    class func lightGrey() -> UIColor {
        return UIColor(red: 38.0/255.0, green: 38.0/255.0, blue: 38.0/255.0, alpha: 1.0)
    }
}

