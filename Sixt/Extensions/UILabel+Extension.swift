//
//  UILabel+Extension.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/4/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import UIKit

extension UILabel {
    func setBackgroundColor(withColor: UIColor) {
        if let textString = self.text {
            self.layoutIfNeeded()
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.backgroundColor,value: UIColor.primary(), range:NSMakeRange(0, attributedString.length))
            self.attributedText = attributedString
        }
    }
    func setSperatorLine(withColor: UIColor)
    {
        if(self.calculateMaxLines() > 1)
        {
            let lineView = UIView(
                   frame: CGRect(x: 0,
                       y: self.bounds.size.height / 2,
                       width: self.bounds.size.width,
                       height: 2
                   )
               )
            lineView.backgroundColor = withColor
            if(!self.subviews.contains(lineView))
            {
                self.addSubview(lineView)
            }
        }
    }
    func calculateMaxLines() -> Int {
        
         let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
         let charSize = font.lineHeight
         let text = (self.text ?? "") as NSString
         let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
         let linesRoundedUp = Int(ceil(textSize.height/charSize))
         print(linesRoundedUp)
         return linesRoundedUp
     }
}
