//
//  Collection.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/4/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation

extension Collection {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
