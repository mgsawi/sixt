//
//  Router.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/1/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Alamofire

enum Router: URLRequestConvertible {
   
    static var baseUrl = "https://cdn.sixt.io"
    case listCars(parameters: Parameters)
    
    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    var params: ([String: Any]?) {
        switch self {
        case .listCars(let parameters):
            return (nil)
        }
    }
    var path: String {
        switch self {
        case .listCars:
            return "codingtask/cars"
        }
    }
    var accessTokenEnabled: Bool
    {
        switch self {
            default:
                return false
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try Router.baseUrl.asURL()

       var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        switch self {
        case .listCars(let parameters):
            urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        }
        urlRequest.httpMethod = method.rawValue
        urlRequest.cachePolicy = .reloadIgnoringLocalCacheData
        urlRequest.timeoutInterval = 30
    
        return urlRequest
    }

}
