//
//  AccessTokenAdapter.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/1/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Alamofire

class AccessTokenAdapter: RequestAdapter {
  
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        
        if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(Router.baseUrl) {
            let Token = UserDefaults.standard.string(forKey: "OAuthToken") ?? ""
            urlRequest.setValue("Bearer " + Token, forHTTPHeaderField: "Authorization")
            urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
            
        }
        
        return urlRequest
    }
}
