//
//  MapModuleApi.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/1/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation
import Alamofire

struct CarListingApi {
    

    func fetch(with parameters: Parameters, onSuccess: @escaping ([CarListingEntity]) -> Void, onError: @escaping (Error) -> Void) {
            let api = RestApiManager(request: Router.listCars(parameters: parameters))
            api.TestingEnabled = true
        
            api.Request(completionHandler: {
                (success,data,error) -> Void in
                if success {
                   do {
                        let response = try self.parse(data)
                        onSuccess(response)
                    } catch {
                        
                        onError(error)
                    }
                }
                else {
                    onError(error)
                }
            })
    }

    private func parse(_ data: Data) throws -> [CarListingEntity] {
        do {
           // let products = try JSONDecoder()
            //    .decode([CarsListingResponse].self, from: data)
            
            let response: [CarListingEntity] = try JSONDecoder().decode([CarListingEntity].self, from: data)
            return response
        }
        catch
        {
            print(error)
        }
        return [CarListingEntity]()
    }
}
