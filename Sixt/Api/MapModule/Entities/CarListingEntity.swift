//
//  CarListingEntity.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/1/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation

struct CarListingEntity: Codable {
  
    var licensePlate: String?
    var name: String?
    var modelName: String?
    var modelIdentifier: String?
    var make: String?
    var id: String?
    var group: String?
    var innerCleanliness: String?
    var longitude: Float?
    var transmission: String?
    var carImageUrl: String?
    var color: String?
    var fuelType: String?
    var fuelLevel: Float?
    var latitude: Float?
    var series: String?

    private enum CodingKeys: String, CodingKey {
        case licensePlate
        case name
        case modelName
        case modelIdentifier
        case make
        case id
        case group
        case innerCleanliness
        case longitude
        case transmission
        case carImageUrl
        case color
        case fuelType
        case fuelLevel
        case latitude
        case series
    }
}

struct CarsListingResponse: Codable {
    let items: [CarListingEntity]
}
