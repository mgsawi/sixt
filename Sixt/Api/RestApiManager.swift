//
//  RestApiManager.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/1/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Alamofire

public enum ApiError: Int, Error {
      case recieveNilResponse = 0,
      recieveErrorHttpStatus,
      recieveNilBody,
      failedParse,
      none
  }

class RestApiManager
{
    typealias CompletionHandler = (_ success:Bool, _ data: Data, _ error: Error) -> Void

 
    var TestingEnabled = false
    var PrintStatusEnabled = false
    var AutoReachEnabled = false
    
    var request: URLRequestConvertible
    
    public init(request: URLRequestConvertible)
    {
        self.request = request
    }
  
    func Request(completionHandler: @escaping CompletionHandler) -> DataRequest
    {
        
        let sessionRequest = Alamofire.request(self.request).responseString{ response in
            do{
            if response.result.value != nil {
                completionHandler(true,response.data ?? Data(), ApiError.none)
            }
        else
            {
                completionHandler(false,Data(), ApiError.recieveNilResponse)
            }
        }
    }
        return sessionRequest
    }
   
    
    func DisplayRequestData(response: DataResponse<Any>)
    {
            debugPrint(response)
            print(response)
            print(response.request!)  // original URL request
            print(response.request?.httpBody ?? "Undefined httpBody")
            print(response.result.value ?? "Undefined resultValue")
    }
    

    
    
    
    
}
