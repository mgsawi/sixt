//
//  GMSViewDelegate.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/4/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation
import GoogleMaps

protocol GMSViewDelegate: AnyObject {

    func mapView(mapView: GMSMapView, marker: GMSMarker)
}
