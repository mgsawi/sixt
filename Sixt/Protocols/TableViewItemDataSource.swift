//
//  TableViewItemDataSource.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/1/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import Foundation
import UIKit

protocol TableViewItemDataSource: AnyObject {
    var numberOfItems: Int { get }

    func itemCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
    func didSelect(tableView: UITableView, indexPath: IndexPath)
}
