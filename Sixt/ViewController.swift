//
//  ViewController.swift
//  Sixt
//
//  Created by MOHAMED GAMAL on 11/2/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        MapRouterInput().present(from: self, entryEntity: MapEntryEntity())

    }
}

extension ViewController: Viewable {}
