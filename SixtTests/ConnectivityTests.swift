 //
//  ConnectivityTests.swift
//  SixtTests
//
//  Created by MOHAMED GAMAL on 11/6/19.
//  Copyright © 2019 Mohamed Gamal. All rights reserved.
//

import XCTest
@testable import Sixt

class ConnectivityTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
                  testCarListingApi()
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCarListingApi()
    {
        let expectation = self.expectation(description: "Fetching")
        CarListingApi().fetch(with: [:], onSuccess: { [weak self] res in
                           expectation.fulfill()
                        }) { [weak self] error in
                            expectation.fulfill()
                           XCTFail("Error: \(error.localizedDescription)")
                        }
        waitForExpectations(timeout: 30, handler: nil)

    }

}
